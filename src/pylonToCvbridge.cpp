#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Int64.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

#include <pylon/PylonIncludes.h>

using namespace std;
using namespace cv;
using namespace Pylon;

static const uint32_t c_countOfImagesToGrab = 800;

class passImages
{
  public:
  passImages()
    : it_(nh)
  {
    // ros interfaces
    pubImage = it_.advertise("pylon_image", 1);

    // Before using any pylon methods, the pylon runtime must be initialized. 
    PylonInitialize();

    // Create an instant camera object with the camera device found first.
    CInstantCamera camera( CTlFactory::GetInstance().CreateFirstDevice());

    // Print the model name of the camera.
    cout << "Using device " << camera.GetDeviceInfo().GetModelName() << endl;

    // The parameter MaxNumBuffer can be used to control the count of buffers
    // allocated for grabbing. The default value of this parameter is 10.
    camera.MaxNumBuffer = 5;

    // Start the grabbing of c_countOfImagesToGrab images.
    // The camera device is parameterized with a default configuration which
    // sets up free-running continuous acquisition.
    camera.StartGrabbing( c_countOfImagesToGrab);

    // This smart pointer will receive the grab result data.
    CGrabResultPtr ptrGrabResult;

    CImageFormatConverter fc;
    fc.OutputPixelFormat = PixelType_BGR8packed;
    CPylonImage image;

    // Buffer images to save
    vector<Mat> imagesToSave;

    cout<<"starting to get images"<<endl;

    // Camera.StopGrabbing() is called automatically by the RetrieveResult() method
    // when c_countOfImagesToGrab images have been retrieved.
    while ( camera.IsGrabbing())
    {
      // Wait for an image and then retrieve it. A timeout of 5000 ms is used.
      camera.RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException);

      // Image grabbed successfully?
      if (ptrGrabResult->GrabSucceeded())
      {
	      fc.Convert(image, ptrGrabResult);
	      Mat img = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,(uint8_t*)image.GetBuffer());
	      //imshow("show",img);  // display the image in OpenCV image window
	      //waitKey(1);

        imagesToSave.push_back(img);

	      sensor_msgs::Image img_msg;
	      cv_bridge::CvImage img_bridge = cv_bridge::CvImage(std_msgs::Header(), sensor_msgs::image_encodings::RGB8, img);
	      img_bridge.toImageMsg(img_msg);
	      pubImage.publish(img_msg);
      }
      else
      {
        cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
	      break;
      }
    }

    cout<<"starting to save images"<<endl;

    // Save images
    int imgNumber = 0;
    for(vector<Mat>::iterator it = imagesToSave.begin(); it != imagesToSave.end(); ++it)
    {
      char imgFile[100];
      sprintf(imgFile,"%09d.png",imgNumber);
      imwrite(imgFile,*it);
      imgNumber++;
    }

    cout<<"finished saving images"<<endl;

    // Clean up
    PylonTerminate();
  }

  private:
    ros::NodeHandle nh;
    image_transport::ImageTransport it_;
    image_transport::Publisher pubImage;
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pylon_to_cvbridger");
  passImages passImagesObj;
  ros::spin();
}

